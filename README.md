# Using application.yml file within the jar for configuration.
java -jar config-frm-yml-file-0.0.1-SNAPSHOT.jar

# Using application.yml file outside of the the jar for configuration. 
java -jar config-frm-yml-file-0.0.1-SNAPSHOT.jar --spring.config.location=/home/greenhon/Documents/development/config-frm-yml-file/application.yml