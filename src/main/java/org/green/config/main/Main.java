package org.green.config.main;

import lombok.extern.slf4j.Slf4j;
import org.green.config.map.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Main implements CommandLineRunner {

    @Autowired
    private MyConfig myConfig;

    @Override
    public void run(String... args) throws Exception {
        myConfig.getApis().forEach(s -> {
                    log.info("API: {}", s);
                }
        );

        myConfig.getConvertors().forEach(s -> {
                    log.info("Converter: bean: {}, locationToSaveFile: {}", s.getBean(), s.getLocationToSaveFile());
                }
        );

        myConfig.getWithHyphen().forEach(s -> {
                    log.info("Property with hypen: {} ", s);
                }
        );
    }
}
