package org.green.config;

import org.green.config.map.MyConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(MyConfig.class)
public class ConfigFrmYmlFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigFrmYmlFileApplication.class, args);
	}

}
