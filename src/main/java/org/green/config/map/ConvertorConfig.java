package org.green.config.map;

import lombok.Data;

@Data
public class ConvertorConfig {
    private String bean;
    private String locationToSaveFile;
}