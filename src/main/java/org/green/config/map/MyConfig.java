package org.green.config.map;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "map.config")
@Getter
@Setter
public class MyConfig {
    List<ConvertorConfig> convertors = new ArrayList<>();
    List<String> apis = new ArrayList<>();
    List<String> withHyphen = new ArrayList<>();
}